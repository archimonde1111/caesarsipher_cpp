#include "main.h"


int main()
{
    string message = "";
	int ValueToMove = 0;

	cout << "Write message to encrypt" << endl;
	while(message.empty())
	{
		getline(cin, message);
	}

	cout << "How many letters you want me to move it?" << endl;
	cin >> ValueToMove;
	while(cin.fail())
	{
		cin.clear();
		cin.ignore(256, '\n');
		cin >> ValueToMove;
	}

	time_t start = clock();

	string encryptedMessage = EncryptWithCaesarCifer(ValueToMove, message);
	cout << "Encrypted message: " << endl;
	cout << encryptedMessage << endl;

	time_t end = clock();
	double duration = double(end - start) / double(CLOCKS_PER_SEC);

	cout << "Encryption time in seconds: \n" << duration << endl;
}

string EncryptWithCaesarCifer(int howManyLettersToMove, const string& messageToEncrypt)
{
	string encryptedMessage = "";

	for(char letter : messageToEncrypt)
	{
		int LetterASCIINumber = static_cast<int> (letter);
		LetterASCIINumber += howManyLettersToMove;

		CalculateASCIIValue(MinASCIIValue, MaxASCIIValue, LetterASCIINumber);

		char encryptedLetter = static_cast<char> (LetterASCIINumber);
		encryptedMessage += encryptedLetter;
	}

	return encryptedMessage;
}
void CalculateASCIIValue(int minASCIIValue, int maxASCIIValue, int& OUTvalueToCorrect)
{
	while(OUTvalueToCorrect > maxASCIIValue)
	{
		int DeltaRisingValue = OUTvalueToCorrect - maxASCIIValue;
		OUTvalueToCorrect = minASCIIValue + DeltaRisingValue;
	}
}