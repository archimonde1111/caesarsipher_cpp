#define MinASCIIValue 32
#define MaxASCIIValue 126

#include <iostream>
#include <string>
#include "time.h"

using namespace std;


string EncryptWithCaesarCifer(int, const string& messageToEncrypt);
void CalculateASCIIValue(int, int, int&);